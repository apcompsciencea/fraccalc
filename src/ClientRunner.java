/*
* Joshua Brown
* Fraction Calculator
*/
import java.util.Scanner;

public class ClientRunner{

    //Initialize all variables for storing and manipulating Frac1, Frac2 and FinalFrac

    private static String frac1String;
    private static int wholeNumber1;
    private static int numerator1;
    private static int denominator1;

    private static String frac2String;
    private static int wholeNumber2;
    private static int numerator2;
    private static int denominator2;

    private static String finalFrac;
    private static int finalWholeNumber;
    private static int finalNumerator;
    private static int finalDenominator;

    private static String operator = "";

    // ******************* Do not change main. ********************
    // This is the default entry into you application.
    // ************************************************************
    public static void main(String[] args) {
        Scanner inputScanner = new Scanner(System.in);
        System.out.println(">");
        String inputLine = inputScanner.nextLine();
        while (!inputLine.equals("quit")) {
            System.out.println(produceAnswer(inputLine));
            System.out.println(">");
            inputLine = inputScanner.nextLine();
        }
    }

    // *************** Do not remove this function. ***************
    // This function is needed for grading. It is for automated
    //  testing. If you remove it, you will be marked down.
    // ************************************************************
    public static String produceAnswer(String inputLine) {
        return produceAnswerFinal(inputLine);
    }

// ************************************************************
// Your code below here.
// ************************************************************
    public static String produceAnswerFinal(String input){
        //Put input string into separate strings for Frac1, Frac2 and Operator
        splitEquation(input);
        //Reset final numbers
        finalWholeNumber = 0;
        finalNumerator = 0;
        finalDenominator = 0;
        //Parse numerator, denominator and whole number from frac strings
        wholeNumber1 = parseWholeNumber(frac1String);
        numerator1 = parseNumerator(frac1String);
        denominator1 = parseDenominator(frac1String);

        wholeNumber2 = parseWholeNumber(frac2String);
        numerator2 = parseNumerator(frac2String);
        denominator2 = parseDenominator(frac2String);
        //Turn to improper Fraction and consolidate negatives into numerator
        numerator1 = turnToImproper(wholeNumber1, numerator1, denominator1);
        numerator2 = turnToImproper(wholeNumber2, numerator2, denominator2);
        //After negatives have been consolidated to numerator, remove all negatives and reset whole number
        wholeNumber1 = 0;
        denominator1 = Math.abs(denominator1);

        wholeNumber2 = 0;
        denominator2 = Math.abs(denominator2);
        //Detect and run operation
        if(operator.equals("+")){
            addFracs();
        } else if(operator.equals("-")){
            subtractFracs();
        } else if(operator.equals("*")){
            multiplyFracs();
        } else if(operator.equals("/")){
            divideFracs();
        }

        //Turn from improper fraction to proper and move negative to front of fraction.
        simplify();
        //Parse fraction pieces back to string to return
        if(finalNumerator == 0){
            //If there is no fraction return only a whole number
            finalFrac = String.valueOf(finalWholeNumber);
        } else if(finalWholeNumber == 0){
            //Don't include the whole number if there is no whole number
            finalFrac = String.valueOf(finalNumerator) + '/' + String.valueOf(finalDenominator);
        } else {
            //Parse all pieces together if all are present
            finalFrac = String.valueOf(finalWholeNumber) + '_' + String.valueOf(finalNumerator) + '/' + String.valueOf(finalDenominator);
        }
        return finalFrac;
    }

    private static int turnToImproper(int wholeNumber, int numerator, int denominator) {
        //create variable 'sign' to contain the sign of the numerator
        int sign = 1;

        if(wholeNumber < 0){
            sign *= -1;
        }
        if(numerator < 0){
            sign *= -1;
        }
        if(denominator < 0){
            sign *= -1;
        }
        //move sign into numerator
        return sign * (Math.abs(numerator) + (Math.abs(wholeNumber) * Math.abs(denominator)));
    }

    public static void splitEquation(String equationString) {
        Scanner equationScanner = new Scanner(equationString);
        //Pull string for Frac1 from equation (Start of line to first space/end of first frac)
        frac1String = equationScanner.next();
        //Pull operator string (End of Frac1 to next space)
        operator = equationScanner.next();
        //Pull string for Frac2
        frac2String = equationScanner.next();
    }
    
    public static int parseWholeNumber(String frac){
        //Find where whole number ends
        int pos = frac.indexOf('_');
        //If there is no underscore whole number is non exsistent and therefore is equal to zero
        if(pos == -1){
            return 0;
        }
        //Return int version of the start to the first _ (whole number)
        return Integer.parseInt(frac.substring(0, pos));
    }
    
    public static int parseNumerator(String frac){
        //Find where the numerator starts and ends
        int startPos = frac.indexOf('_') + 1;
        int endPos = frac.indexOf('/');
        //if fraction doesn't contain a / then the number is a whole number and the entire string is the numerator
        if(endPos == -1){
            return Integer.parseInt(frac);
        }
        //Parse numerator piece of fraction to integer
        return Integer.parseInt(frac.substring(startPos, endPos));
    }
    
    public static int parseDenominator(String frac){
        //Find where denominator starts
        int startPos = frac.indexOf('/') + 1;
        //If there is no denominator then the fraction is a whole number with a denom of 1
        if(startPos == 0){
            return 1;
        }
        //Parse end of frac string to denom
        return Integer.parseInt(frac.substring(startPos));
    }

    public static void addFracs(){
        //Multiply numerator by opisite denominator to apply Common Denominator
        numerator1 *= denominator2;
        numerator2 *= denominator1;

        //Multiply denominators together to get common denominator
        finalDenominator = denominator1 * denominator2;

        //Add numerators together for added fraction
        finalNumerator = numerator1 + numerator2;
    }

    public static void subtractFracs(){
        //Multiply numerator by opisite denominator to apply Common Denominator
        numerator1 *= denominator2;
        numerator2 *= denominator1;

        //Multiply denominators together to get common denominator
        finalDenominator = denominator1 * denominator2;

        //Subtract numerators for subracted fraction
        finalNumerator = numerator1 - numerator2;
    }

    public static void multiplyFracs(){
        finalWholeNumber = 0;

        //Multiply numerators and denominators to apply multiplication
        finalNumerator = numerator1 * numerator2;
        finalDenominator = denominator1 * denominator2;
    }

    public static void divideFracs(){
        //Flips numerator and denominator of second fraction before multiplying

        //Store the numerator of the second  fraction
        int tempNum = numerator2;
        //Store the sign of the second fraction
        int sign = (int) Math.signum(tempNum);

        //Flip numerator and denominator 2 multiplying by the sign to ensure
        //The numerator has the sign not the denominator
        numerator2 = denominator2 * sign;
        denominator2 = tempNum * sign;

        //After flipping the numerator and denominator multiply the fractions
        multiplyFracs();
    }

    public static void simplify(){
        //Store the sign of the final fraction
        int sign = (int) Math.signum(finalNumerator);

        //Remove the sign from the numerator
        finalNumerator = Math.abs(finalNumerator);

        //Work backwards from the largest number the denominator will be divisible by
        //(work from largest to smallest to ensure largest gcd is selected)
        for(int i = Math.abs(finalDenominator); i > 0; i--){

            //If both the numerator are dividable by the test case divide the numerator
            //and denominator by the case then exit the for loop
            if(finalNumerator % i == 0 && finalDenominator % i == 0){
                finalNumerator /= i;
                finalDenominator /= i;

                //Set i to an always false value to exit the loop
                //(Could've used 'break;' but we havent learned that yet)
                i = -1;
            }
        }

        //find how many times the denominator can be taken out of the numerator
        //working backwards to ensure it is taken out as many times as possible
        for(int i = finalNumerator; i > 0; i--){

            //If the test case can be evenly divided by the denominator remove
            //that many units from the numerator
            if(i % finalDenominator == 0){
                //Remove i units from numerator
                finalNumerator -= i;
                //divide i by the denominator the figure out the finalWholeNumber
                finalWholeNumber = (i) / finalDenominator;

                //Set i to an always false value to exit the loop
                //(Could've used 'break;' but we haven't learned that yet)
                i = -1;
            }
        }

        //If there is a whole number apply the sign to it, otherwise apply the sign to the final numerator
        if(finalWholeNumber > 0){
            finalWholeNumber *= sign;
        } else {
            finalNumerator *= sign;
        }
    }
}