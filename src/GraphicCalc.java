import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Point2D;
import java.util.ArrayList;

public class GraphicCalc extends JPanel implements MouseListener {
    private final String[] buttonText = {"C", "±", "/", "÷", "7", "8", "9", "*", "4", "5", "6", "-", "1", "2", "3", "+", "", "0", "⌫", "="};
    private final ArrayList<Shape> buttons = new ArrayList<>();
    private final ArrayList<Boolean> isClicked = new ArrayList<>();
    private String display = "0";
    private String frac1 = "";
    private String operator = "";
    private String frac2 = "";
    private String finalFrac = "";


    public GraphicCalc() {
        for (int i = 0; i < 20; i++) {
            isClicked.add(false);
        }
        for (int i = 155; i <= 395; i += 60) {
            for (int j = 35; j <= 215; j += 60) {
                buttons.add(new Ellipse2D.Double(j, i, 50, 50));
            }
        }
        this.setSize(300, 500);
        addMouseListener(this);
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2d = (Graphics2D) g;
        g2d.setFont(g2d.getFont().deriveFont(20f));
        g2d.setBackground(Color.WHITE);
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2d.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
        int buttonNumb = 0;
        for (int i = 0; i < 20; i++) {
            Shape circle = buttons.get(buttonNumb);
            int x = (int) circle.getBounds().getX();
            int y = (int) circle.getBounds().getY();
            if (isClicked.get(buttonNumb)) {
                g2d.setColor(Color.CYAN);
                g2d.fill(circle);
                g2d.setColor(Color.BLACK);
                g2d.draw(circle);
                g2d.drawString(buttonText[buttonNumb], x + 18, y + 32);
                buttonNumb++;
            } else {
                g2d.setColor(Color.BLACK);
                g2d.fill(circle);
                g2d.setColor(Color.WHITE);
                g2d.drawString(buttonText[buttonNumb], x + 18, y + 32);
                buttonNumb++;
            }
        }

        g2d.setColor(Color.BLACK);
        g2d.drawRect(35, 35, 230, 80);
        g2d.setFont(g2d.getFont().deriveFont(30f));
        g2d.drawString(display, 45, 85);
    }

    @Override
    public void mouseClicked(MouseEvent e) {

    }

    @Override
    public void mousePressed(MouseEvent e) {
        Point2D point = new Point2D.Double(e.getX(), e.getY());
        String clicked = "";
        for (int i = 0; i < 20; i++) {
            if (buttons.get(i).contains(point)) {
                isClicked.set(i, true);
                clicked = buttonText[i];
            }
        }
        if (clicked.equals("±")) {
            if (display.contains("-")) {
                display = display.substring(1, display.length());
            } else {
                display = "-" + display;
            }
        } else if (clicked.equals("/")) {
            if (display.contains("/")) {
                display = display.replace("/", " ");
            }
            display += "/";
        } else if (clicked.equals("C")) {
            display = "0";
            frac1 = "";
            operator = "";
            frac2 = "";
        } else if (clicked.equals("=")) {
            frac2 = display.replace(" ", "_");
            finalFrac = ClientRunner.produceAnswer(frac1 + " " + operator + " " + frac2).replace("_", " ");
            display = finalFrac;
        } else if (clicked.equals("⌫")){
            if(display.substring(display.length() - 1).equals("/")){
                display = display.substring(0, display.length() - 1).replace(" ", "/");
            } else if(display.length() > 1){
                display = display.substring(0, display.length() - 1);
            } else {
                display = "0";
            }
        } else if (clicked.equals("+") || clicked.equals("-") || clicked.equals("*") || clicked.equals("÷")) {
            frac1 = display.replace(" ", "_");
            display = clicked;
            operator = clicked;
        } else if (!clicked.equals("")) {
            if(display.equals("+") || display.equals("-") || display.equals("*") || display.equals("÷") || display.equals("0") || display.equals(finalFrac)){
                display = clicked;
            } else {
                display += clicked;
            }
        }
        revalidate();
        repaint();
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        for (int i = 0; i < 20; i++) {
            isClicked.set(i, false);
        }
        revalidate();
        repaint();
    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }
}
