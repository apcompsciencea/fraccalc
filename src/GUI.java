import javax.swing.*;

public class GUI {
    public static void main(String[] args){
        JFrame frame = new JFrame("Fraction Calculator");
        GraphicCalc graphicCalc = new GraphicCalc();
        graphicCalc.setBounds(0, 0, 300, 500);
        graphicCalc.repaint();

        frame.add(graphicCalc);
        frame.setSize(300, 500);
        frame.setResizable(false);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.revalidate();
        frame.repaint();
        frame.setVisible(true);
    }
}
